import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionsTypesComponent } from './transactions/transactions-types/transactions-types.component';
import { TransactionsTypesService } from './services/transactions-types.service';
import { TransactionsService } from './services/transactions.service';
import { FormsModule } from '@angular/forms';
import { NewTransactionComponent } from './transactions/new-transaction/new-transaction.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TransactionsComponent,
    TransactionsTypesComponent,
    NewTransactionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : [],
    NgbModule.forRoot()
  ],
  providers: [
    TransactionsTypesService,
    TransactionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
