import { TransactionType } from "./transaction-type.model";

export class Transaction {
    public id: number;
    public value: number;
    public date: Date;
    public description: string;
    public transactionType: TransactionType;   
    
    constructor(id: number, value: number, date: Date, description: string, transactionType: TransactionType){
        this.id = id;
        this.value = value;
        this.date = date;
        this.description = description;
        this.transactionType = transactionType;
    }
}