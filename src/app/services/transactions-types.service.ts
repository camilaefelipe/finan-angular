import { EventEmitter } from "@angular/core";
import { TransactionType } from "../models/transaction-type.model";

export class TransactionsTypesService {
    private transactionsTypes: TransactionType[] = [
        new TransactionType(0, 'Comida'),
        new TransactionType(1, 'Transporte')
    ];
    index = 1;
    transactionTypeListEmitter = new EventEmitter<TransactionType[]>();

    getTransactionsTypes() {
        return this.transactionsTypes.slice();
    }

    addTransactionType(transactionType: TransactionType) {
        this.index++;
        transactionType.id= this.index;
        this.transactionsTypes.push(transactionType);
        this.transactionTypeListEmitter.emit(this.transactionsTypes);    
    }
      
    deleteTransactionType(id: number){
        let indexToRemove =0;
        this.transactionsTypes.forEach((transa, index) => {
            if (transa.id === id) {
                indexToRemove =index;
            }
        });
        this.transactionsTypes.splice(indexToRemove, 1);
        this.transactionTypeListEmitter.emit(this.transactionsTypes);
    }

    updateTransactionType(transactionType: TransactionType) {
        this.transactionsTypes.forEach((transa, index) => {
            if (transa.id === transactionType.id) {
                transa.name = transactionType.name;
            }
        });
        this.transactionTypeListEmitter.emit(this.transactionsTypes);
    }

}