import { EventEmitter } from "@angular/core";
import { TransactionType } from "../models/transaction-type.model";
import { Transaction } from "../models/transaction.model";


export class TransactionsService {
    private transactions: Transaction[] = [
        new Transaction(
            1,
            50,
            new Date(),
            'Description 1',
            new TransactionType(1,'Food')
        ),
        new Transaction(
            2,
            19,
            new Date(),
            'Description 1',
            new TransactionType(1,'Transport')
        )
    ];
    
    index = 1;
    transactionListEmitter = new EventEmitter<Transaction[]>();

    getTransactions() {
        return this.transactions.slice();
    }

    addTransaction(transaction: Transaction) {
        this.index++;
        transaction.id = this.index;
        this.transactions.push(transaction);
        this.transactionListEmitter.emit(this.transactions);    
    }
      
    deleteTransaction(id: number){
        let indexToRemove =0;
        this.transactions.forEach((transa, index) => {
            if (transa.id === id) {
                indexToRemove =index;
            }
        });
        this.transactions.splice(indexToRemove, 1);
        this.transactionListEmitter.emit(this.transactions);
    }

    updateTransaction(transaction: Transaction) {
        this.transactions.forEach((transa, index) => {
            if (transa.id === transaction.id) {
                transa = transaction;
            }
        });
        this.transactionListEmitter.emit(this.transactions);
    }
}