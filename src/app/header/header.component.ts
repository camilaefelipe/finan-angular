import { Component, Output, EventEmitter, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Output() menuEvent: EventEmitter<any> = new EventEmitter();
  
  isNavbarCollapsed = false;

  onMenu(feature: string) {
    this.isNavbarCollapsed = false;
    this.menuEvent.emit(feature);
  }
}
