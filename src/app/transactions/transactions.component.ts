import { Component, OnInit } from '@angular/core';
import { Transaction } from '../models/transaction.model';
import { TransactionsService } from '../services/transactions.service';
import { TransactionType } from '../models/transaction-type.model';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  transactions: Transaction[];
  transaction: Transaction;  

  constructor(private transactionsService: TransactionsService) { }

  ngOnInit() {
    this.transactions = this.transactionsService.getTransactions();
    this.transactionsService.transactionListEmitter.subscribe(
      (transactionList: Transaction[])=>{
        console.log(this.transactions);
        this.transactions = transactionList;
      }
    );
  }

  onDeleteTransaction(transaction: Transaction) {
    this.transactionsService.deleteTransaction(transaction.id);
  }

  onEditTransaction(transaction: Transaction) {
    this.transaction = transaction;
  }

}
