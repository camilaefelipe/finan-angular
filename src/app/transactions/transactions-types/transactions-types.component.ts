import { Component, OnInit } from '@angular/core';
import { TransactionType } from '../../models/transaction-type.model';
import { TransactionsTypesService } from '../../services/transactions-types.service';
import { Transaction } from '../../models/transaction.model';

@Component({
  selector: 'app-transactions-types',
  templateUrl: './transactions-types.component.html',
  styleUrls: ['./transactions-types.component.css']
})
export class TransactionsTypesComponent implements OnInit {
  transactionsTypes: TransactionType[];
  transactionType: TransactionType = new TransactionType(null, "");

  constructor(private transactionsTypesService: TransactionsTypesService) { }

  ngOnInit() {
    this.transactionsTypes = this.transactionsTypesService.getTransactionsTypes();
    this.transactionsTypesService.transactionTypeListEmitter.subscribe(
      (transactionList: TransactionType[])=>{
        this.transactionsTypes = transactionList;
      }
    );
  }

  onAddTransactionType() {
    
    if (this.transactionType.id) {
      this.transactionsTypesService.updateTransactionType(this.transactionType);
    } else {
      this.transactionsTypesService.addTransactionType(this.transactionType);
    }
    
    this.transactionType = new TransactionType(null, "");;
  }

  onDeleteTransactionType(transactionType: TransactionType) {
    this.transactionsTypesService.deleteTransactionType(transactionType.id);
  }

  onEditTransactionType(transactionType: TransactionType) {
    this.transactionType = transactionType;
  }

}
