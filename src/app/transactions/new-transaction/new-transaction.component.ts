import { Component, OnInit } from '@angular/core';
import { TransactionsTypesService } from '../../services/transactions-types.service';
import { TransactionType } from '../../models/transaction-type.model';
import { TransactionsService } from '../../services/transactions.service';
import { Transaction } from '../../models/transaction.model';

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.css']
})
export class NewTransactionComponent implements OnInit {
  transaction: Transaction = new Transaction(null, null, null,null,null);
  transactionTypes : TransactionType[];
  constructor(private transactionTypeService: TransactionsTypesService, private transactionService: TransactionsService) { }

  ngOnInit() {
    this.transactionTypes = this.transactionTypeService.getTransactionsTypes();
    this.transactionTypeService.transactionTypeListEmitter.subscribe(
      (transactionTypes : TransactionType[])=> {
        this.transactionTypes = transactionTypes;
      }
    );
  }

  onAddTransaction() {
    if (this.transaction.id) {
      this.transactionService.updateTransaction(this.transaction);

    } else {
      this.transactionService.addTransaction(this.transaction);
    }
    
    this.transaction = new Transaction(null, null, null,null,null);
  }

}
